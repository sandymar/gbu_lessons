﻿using System.Drawing;

namespace GBU_lesson
{
    interface ICollision
    {
        bool Collision(ICollision obj);
        Rectangle Rect { get; }
    }
}
