﻿using System.Drawing;


namespace GBU_lesson
{
    class Asteroid : BaseObject
    {
        public Asteroid(Point dir, Size size, Image image) : base(dir, size, image) { InitStartPosition(); }
        public Asteroid(Point pos, Point dir, Size size) : base(pos, dir, size){}
        public Asteroid(Point pos, Point dir, Image image) : base(pos, dir, image){}
        public Asteroid(Point pos, Point dir, Size size, Image image) : base(pos, dir, size, image){}

        public override void Draw()
        {
            if (_image == null)
            {
                Game.Buffer.Graphics.DrawEllipse(Pens.White, _pos.X, _pos.Y, _size.Width, _size.Height);
            }
            else
            {
                Game.Buffer.Graphics.DrawImage(_image,_pos.X, _pos.Y);
            }
        }


        public override void Update()
        {
            //проверка позиции с поправкой на размер объекта
            int rightPos = _pos.X + _size.Width;
            int bottomPos = _pos.Y + _size.Height;

            //дополнительно корректируем позции по x и y для исклчюение "заползания за экран"
            if (_pos.X <= 0)
            {
                _dir.X = -_dir.X;
                _pos.X = 0;
            }

            if (rightPos >= GameService.FORM_WIDTH)
            {
                _dir.X = -_dir.X;
                _pos.X = GameService.FORM_WIDTH - _size.Width;
            }

            if (_pos.Y <= 0)
            {
                _dir.Y = -_dir.Y;
                _pos.Y = 0;
            }
            if (bottomPos >= GameService.FORM_HEIGHT)
            {
                _dir.Y = -_dir.Y;
                _pos.Y = GameService.FORM_HEIGHT - _size.Height;
            }

            _pos.X = _pos.X + _dir.X;
            _pos.Y = _pos.Y + _dir.Y;

            _image.RotateFlip(RotateFlipType.Rotate90FlipXY);
        }

        public override void InitStartPosition()
        {
            _pos = new Point(GameService.Randomize.Next(100, GameService.FORM_WIDTH), GameService.Randomize.Next(40, GameService.FORM_HEIGHT));
        }
    }
}