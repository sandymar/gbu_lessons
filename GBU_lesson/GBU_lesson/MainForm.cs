﻿using System.Drawing;
using System.Windows.Forms;

namespace GBU_lesson
{
    class MainForm :Form
    {

        private Button btnNewGame, btnRecords, btnExit;
        private Label lbTitle, lbAuthor;

        ///<summary>
        ///Класс основной игровой формы
        ///</summary>
        public MainForm() : base() {

            initForm();
        }

        ///<summary>
        ///Инициализирует и показывает основную игровую форму
        ///</summary>
        private void initForm()
        {
            this.Text = "Asteroids game";
            //Именно ClientSize, т.к. size - размер вместе с заголовками
            //Отключаем изменение размеров пользователем и центрируем окно
            this.ClientSize = new Size(GameService.FORM_WIDTH, GameService.FORM_HEIGHT);
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;

            //готовим управляющие элементы меню            
            btnNewGame = CreateNewButton("Новая игра",0,0);
            btnRecords = CreateNewButton("Рекорды", 50, 0);
            btnExit    = CreateNewButton("Выход", 100, 0);
            btnNewGame.Click += (s,e) => 
            {
                SplashScreen.Stop();
                showInterface(false);
                Game.init(this);
                Game.Draw();
            };
            btnRecords.Click += (s, e) => MessageBox.Show("Тут однажды будет таблица рекордов");
            btnExit.Click += (s, e) => Application.Exit();

            lbTitle = new Label();
            lbTitle.Text = "Астероиды";
            lbTitle.Font = new Font(lbTitle.Font.FontFamily, 36, FontStyle.Bold);
            lbTitle.TextAlign = ContentAlignment.MiddleCenter;
            lbTitle.ForeColor = Color.Coral;
            lbTitle.BackColor = Color.Black;
            lbTitle.Size = new Size(300, 40);
            lbTitle.Left = GameService.FORM_WIDTH / 2 - (lbTitle.Size.Width / 2);
            lbTitle.Top  = GameService.FORM_HEIGHT / 2 - (lbTitle.Size.Height*3);

            lbAuthor = new Label();
            lbAuthor.Text = "Автор: SandyMar";
            lbAuthor.Font = new Font(lbTitle.Font.FontFamily, 12);
            lbAuthor.TextAlign = ContentAlignment.MiddleCenter;
            lbAuthor.ForeColor = Color.Coral;
            lbAuthor.BackColor = Color.Black;
            lbAuthor.Size = new Size(300, 40);
            lbAuthor.Left = GameService.FORM_WIDTH / 2 - (lbAuthor.Size.Width / 2);
            lbAuthor.Top  = GameService.FORM_HEIGHT - 40;

            this.Controls.Add(btnNewGame);
            this.Controls.Add(btnRecords);
            this.Controls.Add(btnExit);
            this.Controls.Add(lbTitle);
            this.Controls.Add(lbAuthor);

            this.BackColor = Color.Black;
            this.ShowIcon = false;
            this.Show();

            showInterface(true);
        }

        /// <summary>
        /// Возвращает новую кнопку интерфейса
        /// </summary>
        /// <param name="text">Текст кнопки</param>
        /// <param name="centerMarignY">Отступ от центра по Y</param>
        /// <param name="centerMarignX">Отступ от центра по X</param>
        /// <returns></returns>
        private Button CreateNewButton(string text, int centerMarignY, int centerMarignX)
        {
            Button btnNew = new Button();
            btnNew.Size = new Size(150, 40);
            btnNew.Left = GameService.FORM_WIDTH / 2 - (btnNew.Size.Width / 2) + centerMarignX;
            btnNew.Top = GameService.FORM_HEIGHT / 2 - (btnNew.Size.Height / 2) + centerMarignY;
            btnNew.Text = text;
            btnNew.FlatStyle = FlatStyle.Flat;
            btnNew.BackColor = Color.LightBlue;
            return btnNew;
        }

        /// <summary>
        /// Отобразить/скрыть интерфейс приложения
        /// </summary>
        /// <param name="state">Признак видимости интерфейса</param>
        private void showInterface(bool state)
        {
            btnExit.Enabled = state;
            btnExit.Visible = state;

            btnNewGame.Enabled = state;
            btnNewGame.Visible = state;

            btnRecords.Enabled = state;
            btnRecords.Visible = state;

            lbAuthor.Enabled = state;
            lbAuthor.Visible = state;

            lbTitle.Enabled = state;
            lbTitle.Visible = state;
        }

    }
}