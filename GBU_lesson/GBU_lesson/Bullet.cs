﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBU_lesson
{
    class Bullet : BaseObject
    {
        public Bullet(Point dir, Size size, Image image) : base(dir, size, image) { InitStartPosition(); }
        public Bullet(Point pos, Point dir, Size size) : base(pos, dir, size) { }
        public Bullet(Point pos, Point dir, Image image) : base(pos, dir, image) { }
        public Bullet(Point pos, Point dir, Size size, Image image) : base(pos, dir, size, image) { }

        public override void Draw()
        {
            if (_image == null)
            {
                Game.Buffer.Graphics.DrawLine(Pens.White, _pos.X, _pos.Y, _size.Width, _size.Height);
            }
            else
            {
                Game.Buffer.Graphics.DrawImage(_image, _pos.X, _pos.Y);
            }
        }

        public override void Update()
        {
            _pos.X = _pos.X + _dir.X;
            if (_pos.X>GameService.FORM_WIDTH) InitStartPosition();
        }

        public override void InitStartPosition()
        {
            _pos = new Point(-20, GameService.Randomize.Next(20, GameService.FORM_HEIGHT - 20));
        }
    }
}
