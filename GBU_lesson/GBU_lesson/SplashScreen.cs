﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GBU_lesson
{
    class SplashScreen
    {

        public static BaseObject[] _objs;
        private static BufferedGraphicsContext _context;
        public static BufferedGraphics Buffer;

        public static int Width { get; set; }
        public static int Height { get; set; }
        private static Timer timer;

        static SplashScreen(){}

        /// <summary>
        /// Инициализация формы заставки
        /// </summary>
        /// <param name="form">Фоорма-родитель</param>
        public static void init(Form form)
        {
            Graphics g;
            _context = BufferedGraphicsManager.Current;
            g = form.CreateGraphics();
            Width = form.ClientSize.Width;
            Height = form.ClientSize.Height;
            Buffer = _context.Allocate(g, new Rectangle(0, 0, Width, Height));

            Load();

            timer = new Timer();
            timer.Interval = GameService.UPDATE_INTERVAL * 5;
            timer.Tick += onTimerTick;
            timer.Start();
        }

        /// <summary>
        /// Отрисовка игровых элементов на экране
        /// </summary>
        public static void Draw()
        {
            Buffer.Graphics.Clear(Color.Black);
            foreach (BaseObject obj in _objs)
            {
                obj.Draw();
            }
            Buffer.Render();
        }

        /// <summary>
        /// Инициализация игровых объектов
        /// </summary>
        public static void Load()
        {
            _objs = new BaseObject[60];

            for (int i = 0; i < _objs.Length; i++)
            {
                Point pos = new Point(GameService.Randomize.Next(10, GameService.FORM_WIDTH), GameService.Randomize.Next(10, GameService.FORM_HEIGHT));
                Point dir = new Point(0, 0);
                SplashAnimation splashAnimation = new SplashAnimation(pos, dir, new Size(20, 20), Properties.Resources.imgStar);
                _objs[i] = splashAnimation;
            }

        }

        /// <summary>
        /// Обновление игрового экрана
        /// </summary>
        public static void Update()
        {
            foreach (BaseObject obj in _objs)
            {
                obj.Update();
            }
        }

        public static void onTimerTick(object sender, EventArgs e)
        {
            Draw();
            Update();
        }

        public static void Stop()
        {
            timer.Stop();
            //оставим разбираться с этим сборщику мусора
            _objs = null;
            _context = null;
            Buffer = null;
        }
    
    }
}
