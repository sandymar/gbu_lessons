﻿using System;

namespace GBU_lesson
{
    ///<summary>
    ///Класс используется как хранилище констант и универсальных методов.
    ///</summary>
    public static class GameService
    {
        ///<summary>Глобальная константа: высота формы</summary>
        public const int FORM_HEIGHT = 600;
        ///<summary>Глобальная константа: ширина формы</summary>
        public const int FORM_WIDTH = 800;
        ///<summary>Глобальная константа: интервал обновления формы</summary>
        public const int UPDATE_INTERVAL = 80;
        ///<summary>Экземпляр класса Random</summary>
        public static Random Randomize = new Random();

    }
}