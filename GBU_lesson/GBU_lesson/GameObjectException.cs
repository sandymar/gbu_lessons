﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBU_lesson
{
    class GameObjectException:Exception
    {
        /// <summary>
        /// Возвращает исключение некорректного создания игрового объекта
        /// </summary>
        /// <param name="message">Текст сообщения</param>
        public GameObjectException(string message) : base(message) { }
    }
}
