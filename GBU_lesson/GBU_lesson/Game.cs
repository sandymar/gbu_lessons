﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace GBU_lesson
{
    static class Game
    {
        public static BaseObject[] _objs;
        public static Bullet _bullet;
        private static BufferedGraphicsContext _context;
        public static BufferedGraphics Buffer;

        public static int Width { get; set; }
        public static int Height { get; set; }
        private static Timer timer;

        static Game() {}

        /// <summary>
        /// Инициализация формы игрового экрана
        /// </summary>
        /// <param name="form">Фоорма-родитель</param>
        public static void init(Form form) {
            Graphics g;
            _context = BufferedGraphicsManager.Current;
            g = form.CreateGraphics();
            Width = form.ClientSize.Width;
            Height = form.ClientSize.Height;

            if (Width > 1000) { throw new ArgumentOutOfRangeException(); }
            else if (Height > 1000) { throw new ArgumentOutOfRangeException(); }
            else if (Width < 0 || Height < 0) { throw new ArgumentOutOfRangeException(); };

            Buffer = _context.Allocate(g, new Rectangle(0, 0, Width, Height));

            Load();

            timer = new Timer();
            timer.Interval = GameService.UPDATE_INTERVAL;
            timer.Tick += onTimerTick;
            timer.Start();
        }

        /// <summary>
        /// Отрисовка игровых элементов на экране
        /// </summary>
        public static void Draw()
        {
            Buffer.Graphics.Clear(Color.Black);
            Buffer.Graphics.DrawImage(Properties.Resources.imgBackgroung, new Rectangle(0,0,Width,Height));
            foreach (BaseObject obj in _objs)
            {
                obj.Draw();
            }
            _bullet.Draw();
            Buffer.Render();
        }

        /// <summary>
        /// Инициализация игровых объектов
        /// </summary>
        public static void Load()
        {
            _objs = new BaseObject[60];

            for (int i = 0; i < _objs.Length / 4*3; i++)
            {
                Point dir = new Point(GameService.Randomize.Next(-16,-8),0);
                Image imgStar = Properties.Resources.imgStar;
                _objs[i] = new Star(dir, new Size(20, 20), imgStar);
            }

            for (int i = _objs.Length / 4*3; i < _objs.Length; i++)
                {
                Point dir = new Point(GameService.Randomize.Next(-3, 3), GameService.Randomize.Next(-10, 10));
                Image imgAsteroid = Properties.Resources.imgAsteroid;
                _objs[i] = new Asteroid(dir, new Size(45, 45), imgAsteroid);
            }

            Image imgBullet = Properties.Resources.imgBullet;
            _bullet = new Bullet(new Point(20, 0), new Size(20, 20), imgBullet);
        }

        /// <summary>
        /// Обновление игрового экрана
        /// </summary>
        public static void Update()
        {
            foreach (BaseObject obj in _objs)
            {
                obj.Update();
                if (obj is Asteroid) {
                    if (obj.Collision(_bullet)) {
                        _bullet.InitStartPosition();
                        obj.InitStartPosition();
                        System.Media.SystemSounds.Hand.Play(); };

                }
            }
            _bullet.Update();
        }

        public static void onTimerTick(object sender, EventArgs e)
        {
            Draw();
            Update();
        }

        public static void Stop()
        {
            timer.Stop();
            _objs = null;
            _context = null;
            Buffer = null;
        }
    }
}
