﻿using System.Drawing;
using System.Drawing.Imaging;

namespace GBU_lesson
{
    class SplashAnimation : BaseObject
    {

        public SplashAnimation(Point dir, Size size, Image image) : base(dir, size, image) { InitStartPosition(); }
        public SplashAnimation(Point pos, Point dir, Size size) : base(pos, dir, size){}
        public SplashAnimation(Point pos, Point dir, Image image) : base(pos, dir, image){}
        public SplashAnimation(Point pos, Point dir, Size size, Image image) : base(pos, dir, size, image){}

        //переопределяем для использования со SplashScreen
        public override void Draw()
        {
            //отрисовка объекта на указанной позиции
            if (_image == null)
            {
                SplashScreen.Buffer.Graphics.DrawEllipse(Pens.White, _pos.X, _pos.Y, _size.Width, _size.Height);
            }
            else
            {
                if (GameService.Randomize.Next(1, 10) == 1) SplashScreen.Buffer.Graphics.DrawEllipse(Pens.Black, _pos.X, _pos.Y, _size.Width, _size.Height);
                else SplashScreen.Buffer.Graphics.DrawImage(_image, _pos.X, _pos.Y);
            }
        }

        public override void Update()
        {
            if (GameService.Randomize.Next(1, 10) == 1) _pos = new Point(GameService.Randomize.Next(10, GameService.FORM_WIDTH), GameService.Randomize.Next(10, GameService.FORM_HEIGHT));
        }

        public override void InitStartPosition() {}

    }
}