﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GBU_lesson
{
    static class Program
    {
        
        [STAThread]
        static void Main()
        {
            MainForm mainForm = new MainForm();
            SplashScreen.init(mainForm);
            SplashScreen.Draw();
            Application.Run(mainForm);
        }
    }
}
