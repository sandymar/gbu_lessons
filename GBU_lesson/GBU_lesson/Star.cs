﻿using System.Drawing;

namespace GBU_lesson
{
    class Star : BaseObject
    {
        public Star(Point dir, Size size, Image image) : base(dir, size, image) { InitStartPosition(); }
        public Star(Point pos, Point dir, Size size) : base(pos, dir, size) { }
        public Star(Point pos, Point dir, Image image) : base(pos, dir, image){}
        public Star(Point pos, Point dir, Size size, Image image) : base(pos, dir, size, image){}

        public override void Draw()
        {
            if (_image == null) {
                Game.Buffer.Graphics.DrawLine(Pens.White, _pos.X, _pos.Y, _pos.X + _size.Width, _pos.Y + _size.Height);
                Game.Buffer.Graphics.DrawLine(Pens.White, _pos.X + _size.Width, _pos.Y, _pos.X, _pos.Y + _size.Height);
            }
            else
            {
                Game.Buffer.Graphics.DrawImage(_image, _pos);
            }
        }

        public override void Update()
        {
            _pos.X = _pos.X + _dir.X;
            //объект должен полностью скрыться за границей экрана
            if (_pos.X+_size.Width < 0) _pos.X = GameService.FORM_WIDTH + _size.Width;
        }

        public override void InitStartPosition()
        {
            _pos = new Point(GameService.Randomize.Next(5, GameService.FORM_WIDTH), GameService.Randomize.Next(5, GameService.FORM_HEIGHT));
        }

    }
}